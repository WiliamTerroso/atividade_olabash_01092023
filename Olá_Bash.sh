#!/bin/bash

# Imprima a frase
echo "Tudo o que um sonho precisa para ser realizado é alguém que acredite que ele possa ser realizado."

# Obtenha o nome do usuário atual
usuario=$USER

# Imprima "Bom dia" e "Bem-vindo" para o usuário
echo "Bom dia, $usuario! Bem-vindo!"

# Obtenha o dia da semana
dia_semana=$(date +%A)

# Imprima o dia da semana
echo "Hoje é $dia_semana."

# Obtenha as informações do clima em João Pessoa usando o wttr.in
clima_joao_pessoa=$(curl -s wttr.in/JoaoPessoa?format=%C+%t)

# Imprima o clima em João Pessoa
echo "O clima em João Pessoa é: $clima_joao_pessoa"

